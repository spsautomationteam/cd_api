class CDApiTest

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"


  #@base_url
  @@reference_num = "1"
  @@content_type = ''
  @@device_type = ''

#  constructor to initialize the config.yaml file
  def initialize()
    config = YAML.load_file('config.yaml')
    @base_url = config['url']
   # @fallback_url = config['fallbackurl']
    # puts "@base_url :#{@base_url}"
    set_url @base_url
  end

#  setter method for authorization
  def set_authorization(mid, mkey)
    @authorization ="Bearer SPS-DEV-GW:test."+mid+"."+mkey
  end

  #  setter method for url
  def set_url(url)
    @url = url
  end

  #  setter method for header
  def set_header(header, value)
    case header
      when "content-type"
        @@content_type =value
      when "deviceType"
        @@device_Type =value
    end
  end

  #  setter method for json body
  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

  # cd api post method handler
  def post(path)
     full_url= @url+path

    #Do post action and get response
    begin
      @response = RestClient.post full_url, @json_body, :content_type => @@content_type, :device_type => @@device_type
      puts "responseBody : #{@response}"

      @@reference_num = JSON.parse(@response.body)['reference']
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::InternalServerError => err1
      @response = err1.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
  end

  def patch(path)
    puts "#patch request"
    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@@content_type: #{@@content_type}"
    puts "@@device_type: #{@@device_type}"
    puts "@@reference_num:#{@@reference_num}"

    #Do post action and get response
    begin
      @response = RestClient.patch full_url+"/"+@@reference_num, @json_body, :Authorization => @authorization, :content_type => @@content_type, :device_type => @@device_type
    rescue RestClient::BadRequest => err
      @response = err.response
    end

    # puts "response: #{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response.code : #{@response.code}"
  end

  # verify api transaction  code
  def verify_txn_code(code)
    expect(@response.code.to_s).to eql(code)
    #,"Body:#{@parsed_response}"
  end

# verify api request status
  def verify_status(status)
    expect(@parsed_response['status']).to eql("Approved"), "Expected : Approved ,got : #{@parsed_response['status']}\n Responesbody: #{@parsed_response}"
    #,"Body:#{@parsed_response}"
  end

# verify cd api all emv tags
  def verify_all_emvtags(tags)
   # puts "val1#{@response.split("EmvTags")[1]}"
   # puts "val2#{@response.split("EmvTags")[1].split( "SwipeData")[0]}"
    expect(@response.include?tags).to be_truthy
  end

  # verify emv tags should be exist
  def verify_emvtag_exist(value)
  #  puts @parsed_response['EmvTags'].length
    expect(@response.include? value).to be_truthy
  end

  # verify emv tags should not be exist
  def verify_emvtag_notexist(tag)
    expect(@response.include? tag).to be_falsey
  end

  # verify cd api specific emv tags tag4F, tag5A, tag50, tag57
  def verify_emvtags(name, value, length)
    # puts "name#{name} ,value:#{value} , length#{length}"
    length_int = length.to_i
  #  puts "length_int#{length_int}"
    i = 0
    loop do
      i += 1
       # puts @parsed_response['EmvTags'][i]['Name']
      if (@parsed_response['EmvTags'][i]['Name'] == name)
        expect(@parsed_response['EmvTags'][i]['Name']).to eql name
        expect(@parsed_response['EmvTags'][i]['Value']).to eql value
        expect(@parsed_response['EmvTags'][i]['Length']).to eql length_int
        break
      end
    end
  end

  #  verify response body parameters for cd api
  def verify_response_params(response_param, value)
   # puts "response_param#### :#{response_param} , value:#{value}"
    if value.include? "null"
      value = nil
    end

    case response_param
      when "code"
        expect(@response.code.to_s).to eql(value), "Expected : #{value} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
      else
        if response_param.include? "."
          response_param1 = response_param.split('.').first
          response_param2 = response_param.split('.').last
        #  puts "response_param1:#{response_param1} ,response_param2#{response_param2}"
          expected_val =@parsed_response[response_param1][response_param2]
          if expected_val.nil?
            expect(expected_val).to eql(value), "Expected : #{value} ,got : #{expected_val} \nResponesbody:#{@parsed_response}\n"
          else
         # puts expected_val.to_s.strip.length
            expect(expected_val.to_s.strip).to eql(value), "Expected : #{value} ,got : #{expected_val.to_s.strip} \nResponesbody:#{@parsed_response}\n"
          end
        else
          expected_val =@parsed_response[response_param]
          if expected_val.nil?
            expect(expected_val).to eql(value), "Expected : #{value} ,got : #{expected_val} \nResponesbody:#{@parsed_response}\n"
          else
            expect(expected_val.delete('')).to eql(value), "Expected : #{value} ,got : #{expected_val.delete('')} \nResponesbody:#{@parsed_response}\n"
          end
        end
    end
  end

#  verify response headers for cd api
  def verify_response_headers(header_param, value)
    key ="";
    case header_param
      when "pragma"
        key =@response.headers[:pragma]
      when "cache-control"
        key =@response.headers[:cache_control]
      when "content-length"
        key =@response.headers[:content_length]
      when "expires"
        key =@response.headers[:expires]
      else

    end
    expect(key).to include(value), "Expected : #{value} ,got : #{key} \nResponesbody:#{@parsed_response}"
#expect(@response.headers[':cache_control']).to include(value), "Expected : #{value} ,got : #{@response.headers[':cache_control']} \nResponesbody:#{@parsed_response}"
  end

  def set_multipleurl(url)
    case url
      when "content-type"
        @@content_type =value
      when "Authorization"
        case value
          when 'DirectMarketing'
            @authorization =@direct_marketing
          when 'DirectMarketingPcl3'
            @authorization=@direct_marketing_pcl3
          else
            @authorization =value
        end
    end
  end

end