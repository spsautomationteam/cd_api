Feature: Card Decrypt API - Verify CD API functionality with IDTECHAUGUSTA_Offline Declined tracking data
  for Visa, Amex, Master, Discover cards

  @post-CDAPI_IDTechAugusta_OfflineDeclined_VisaTrackData
  Scenario: Verify post CDAPI by IDTechAugusta OfflineDeclined TrackData for Visa card
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "DFEE25020001DFEE26022000DFEE120A62994999190003C0023B4F00500B5649534120435245444954DFEF5B084761CCCCCCCC00105A108317DF04C1F806142C7F5D3394A1D9C55F2A0208405F3401015F57010082025C008A00950502B00080009A030001019B02C8009C01009F02060000000001009F03060000000000009F0607A00000000310109F0702AB809F0D0500000000009F0E0500000000009F0F0500000000009F100706010A038000009F1101019F120F4352454449544F20444520564953419F1A0208409F21030000009F2608972D12ADD1E76B439F2701009F34031E03009F360200039F3704CFEAF31F9F390105DF1305DC4000A800DF14050010000000DF1505DC4004F800DFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path SwipeData should be null
    And response body path EmvError should be null
    And response body path DeviceType should be IDTech
    And response body path EmvData.Brand should be VISA CREDIT
    And response body path EmvData.MaskedCardnumber should be 476173XXXXXX0010
    And response body path EmvData.Cardnumber should be 4761739001010010
    And response body path EmvData.ExpirationDate should be null
    And response body path EmvData.CardholderName should be null
    And response body path should not contain "Name": "57"
    And verify all the list of emv tags for Augusta OfflineDecline track data
     | TagName |
 #   | SAGE    |
     | 4F      |
     | 95      |
     | 9F10    |
     | 9B      |
     | 8A      |
     | 50      |
     | 5A      |
     | 5F2A    |
     | 5F34    |
     | 82      |
     | 95      |
     | 9A      |
     | 9C      |
     | 9F02    |
     | 9F03    |
     | 9F07    |
     | 9F0D    |
     | 9F0E    |
     | 9F0F    |
     | 9F10    |
     | 9F12    |
     | 9F1A    |
     | 9F26    |
     | 9F27    |
     | 9F34    |
     | 9F36    |
     | 9F37    |
     | DF13    |
     | DF14    |
     | DF15    |


  @post-CDAPI_IDTechAugusta_OfflineDeclined_InvalidVisaTrackData
  Scenario: Verify post CDAPI by IDTechAugusta OfflineDeclined Invalid TrackData 'DDDDDD' for Visa card
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "DDDDDDDDFEE25020001DFEE26022000DFEE120A62994999190003C0023B4F00500B5649534120435245444954DFEF5B084761CCCCCCCC00105A108317DF04C1F806142C7F5D3394A1D9C55F2A0208405F3401015F57010082025C008A00950502B00080009A030001019B02C8009C01009F02060000000001009F03060000000000009F0607A00000000310109F0702AB809F0D0500000000009F0E0500000000009F0F0500000000009F100706010A038000009F1101019F120F4352454449544F20444520564953419F1A0208409F21030000009F2608972D12ADD1E76B439F2701009F34031E03009F360200039F3704CFEAF31F9F390105DF1305DC4000A800DF14050010000000DF1505DC4004F800DFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400
    And response body path should not contain "SwipeData": null
    And response body path should not contain "EmvError": null
    And response body path should not contain "DeviceType": "IDTech"
    And response body path should not contain "Cardnumber": "4761739001010010"
    And response body path should not contain "MaskedCardnumber": "476173XXXXXX0010"
    And response body path should not contain "ExpirationDate": null
    And response body path should not contain "CardholderName": null
    And response body path should not contain "Brand": "VISA CREDIT"

