Feature: Card Decrypt API - Verify post CDAPI for IDTechAugusta FallBackSwipe due to bad chip read and empty Candidate list

  # ******************* IDTechAugusta_FallBackSwipe_FirstAttempt *****************************

  @post-CDAPI_IDTechAugusta_FallBackSwipe_FirstandSecondAttempts
  Scenario: Verify post CDAPI for IDTechAugusta FallBackSwipe due to bad chip read for First & Second Attempts
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "DFEF6102F220" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path SwipeData should be null
    And response body path EmvData.Brand should be null
    And response body path EmvData.MaskedCardnumber should be null
    And response body path EmvData.Cardnumber should be null
    And response body path EmvData.ExpirationDate should be null
    And response body path EmvError.EmvErrorMessage should be TechnicalError
    And response body path EmvError.FrontEndPrompt should be PromptforInsert
    And response body path DeviceData.KernelVersion should be 312E31302E303337
    And response body path DeviceData.DeviceNumber should be 43234646
    And response body path OfflineDecline should be null
    And response body path should contain "EmvTags":[{"Name":"DFEF61","Length":2,"Value":"F220"}]

  @post-CDAPI_IDTechAugusta_FallBackSwipe_FirstSecondAttempts_NegativeTest_bySmallLetters
  Scenario: Verify post CDAPI for IDTechAugusta FallBackSwipe due to bad chip read for First & Second Attempts by small letters
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "dfef6102f220" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path SwipeData should be null
    And response body path EmvData.Brand should be null
    And response body path EmvData.MaskedCardnumber should be null
    And response body path EmvData.Cardnumber should be null
    And response body path EmvData.ExpirationDate should be null
    And response body path EmvError should be null
    And response body path DeviceData.KernelVersion should be 312E31302E303337
    And response body path DeviceData.DeviceNumber should be 43234646
    And response body path OfflineDecline should be null
    And response body path should contain "EmvTags":[{"Name":"dfef61","Length":2,"Value":"f220"}]

  @post-CDAPI_IDTechAugusta_FallBackSwipe_FirstSecondAttempts_NegativeTest_byInvalidData
  Scenario: Verify post CDAPI for IDTechAugusta FallBackSwipe due to bad chip read for First & Second Attempts by Invalid data
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "TestDFEF6102F220" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400
 #  And response body path should contain no content
    And response body path should not contain "EmvTags":[{"Name":"DFEF61","Length":2,"Value":"F220"}]

    # ******************* IDTechAugusta_FallBackSwipe_ThirdAttempt *****************************

  @post-CDAPI_IDTechAugusta_FallBackSwipe_ThirdAttempt
  Scenario: Verify post CDAPI for IDTechAugusta FallBackSwipe due to bad chip read for Third attempt
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "DFEF6102F222" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path SwipeData should be null
    And response body path EmvData.Brand should be null
    And response body path EmvData.MaskedCardnumber should be null
    And response body path EmvData.Cardnumber should be null
    And response body path EmvData.ExpirationDate should be null
#   And response body path EmvError should be null
    And response body path EmvError.EmvErrorMessage should be TechnicalError
    And response body path EmvError.FrontEndPrompt should be PromptforSwipe
    And response body path DeviceData.KernelVersion should be 312E31302E303337
    And response body path DeviceData.DeviceNumber should be 43234646
    And response body path OfflineDecline should be null
    And response body path should contain "EmvTags":[{"Name":"DFEF61","Length":2,"Value":"F222"}]

  @post-CDAPI_IDTechAugusta_FallBackSwipe_ThirdAttempt_bySmallLetters
  Scenario: Verify post CDAPI for IDTechAugusta FallBackSwipe due to bad chip read for Third attempt by Small Letters
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "dfef6102f222" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path SwipeData should be null
    And response body path EmvData.Brand should be null
    And response body path EmvData.MaskedCardnumber should be null
    And response body path EmvData.Cardnumber should be null
    And response body path EmvData.ExpirationDate should be null
    And response body path EmvError should be null
    And response body path DeviceData.KernelVersion should be 312E31302E303337
    And response body path DeviceData.DeviceNumber should be 43234646
    And response body path OfflineDecline should be null
    And response body path should contain "EmvTags":[{"Name":"dfef61","Length":2,"Value":"f222"}]

  @post-CDAPI_IDTechAugusta_FallBackSwipe_ThirdAttempt_NegativeTest_byInvalidData
  Scenario: Verify post CDAPI for IDTechAugusta FallBackSwipe due to bad chip read for Third attempt by invalid data
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "TestDFEF6102F222" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400
  # And response body path should contain no content
    And response body path should not contain "EmvTags":[{"Name":"DFEF61","Length":2,"Value":"F222"}]

    # ******************* IDTechAugusta_FallBackSwipe_EMVSwipedData ******************************

  @post-CDAPI_IDTechAugusta_FallBackSwipe_EMVSwipedData_FinalAttempt
  Scenario Outline: Verify post CDAPI by for IDTechAugusta FallBackSwipe for final attempt (EMV Swiped Data)
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to <deviceData>
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path EmvData should be null
    And response body path EmvTags should be null
    And response body path SwipeData.HasChip should be true
    And response body path SwipeData.Track1 should be <Track1>
    And response body path SwipeData.Track2 should be <Track2>
    And response body path SwipeData.Cardnumber should be <cardNumber>
    And response body path SwipeData.MaskedCardnumber should be <maskedCardnumber>
    And response body path SwipeData.ExpirationDate should be <expirationDate>
    And response body path SwipeData.FallbackReason should be TechnicalError
  #  And response body path SwipeData.FrontEndPrompt should be ""
    And response body path SwipeData.FrontEndPrompt should be null
  #  And response body path EmvError.EmvErrorMessage should be Emv card swiped
  #  And response body path EmvError.FrontEndPrompt should be PromptforInsert
    And response body path DeviceData.KernelVersion should be 312E31302E303337
  #  And response body path DeviceData.DeviceNumber should be ""
    And response body path OfflineDecline should be null

  Examples:
  | cardName                    | maskedCardnumber | cardNumber       | expirationDate |  Track1   | Track2                                 | deviceData |
  | Visa                        | 417666XXXXXX0019 | 4176662220000019 | 14/12          | null      | ;4176662220000019=14122011000099999991? | {"deviceData": "DFEE250230029F390180DFEE238201CA02BE01801F402800A39B%*4176********0019^PVTDEMOCARD^*******************************?*;4176********0019=********************?*6DB89E7051B95364138906964D61F5AECAEF81534EAD0511B953E0B3F6D3F538BAD5DD1C6DB19383820D528605423E39E60D316CE9D74F9B8E39391E5D411D690427379DA50F1B18F5B8C0F45D98047260E937399E891B92C873D4BF1ED5F759955062484305B53C000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C002A26D6B03"}   |
# | Master Debit                | 512857XXXXXX0002 | 5128570152280002 | 19/08/31       | null      | ;2223000010232012=19122010000000007400? | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994900810008C002A64F07A000000004101050104465626974204D617374657243617264DFEF5D105128CCCCCCCC0002D1908622CCCCCCCC5718FBF7F24F06155FE8EB64EFD1463D085888E00BC844B327A8DFEF5B085128CCCCCCCC00025A10F8BDF7FF2DC5DF112D26943933D1C1FD820239008407A00000000410108701018A025A33950504200080009A030001019B02E8009C01005F24031908315F2A0208405F30005F3401009F02060000000001009F03060000000000009F0607A00000000410109F070229009F080200029F090200029F1101019F0D05BC50BC08009F0E0500000000009F0F05BC70BC98009F12104465626974204D6173746572436172649F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34031E03009F3501219F3602013F9F37042EF30A8C9F38009F3901059F3C009F4005F000F0A0019F4104000004509F5301529F6E009F7C005F201A2F434849502054455354204341524420202020202020202020205F280208405F2D02656E5F5600DFEE2300FFEE01009F260885D23B09817E55149F2701809F10120110A00005220000000000000000000000FFDFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}   |
  | MASTER CARD BIN-2 SERIES    | 222300XXXXXX2012 | 2223000010232012 | 19/12          | null      | ;2223000010232012=19122010000000007400? | {"deviceData": "DFEE250230029F390180DFEE238201C702BB01801F3D2800A39B%*2223********2012^TEST CARD/EMV BIN-2^********************?*;2223********2012=********************?*4EC25EDBE95D82724C88412C6A62FFE8438942DD4D1766F2465A4C464D371EAE060595C1C0FC0073424A8FC9566314CEBC26654FA189C3B6BF1A2269E8E9683A83657A4E692C73A44A76439E3285999E8BF0FE7C94EFDB036C7ECB2485CAD46AECBBA0E23862A505000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C002A35BEF03"}   |
# | American Express            | 374245XXXXX1007  | 374245001741007  | 20/01/31       | null      | ;2223000010232012=19122010000000007400? | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994900810008C002394F08A0000000250108015010414D45524943414E2045585052455353DFEF5D133742CCCCCCC1007D2001201CCCCCCCCCCCCCCC5718F251E8F17B3AA91E1E1AA588042D2DAF1068580FDC1EDFC9DFEF5B083742CCCCCCC1007F5A106E5DE8F2C5E59CEA0AECB74D6032B5AA82023C008408A0000000250108018701018A025A33950508200080009A030001019B02E8009C01005F24032001315F2A0208405F300202015F3401009F02060000000001009F03060000000000009F0606A000000025019F0702FF009F080200019F090200019F1101009F0D05BC50ECA8009F0E0500000000009F0F05BC78FCF8009F12009F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34035E03009F3501219F360200049F3704724905B29F38009F3901059F3C009F4005F000F0A0019F4104000003559F5301529F6E009F7C005F201041454950532032302F56455220322E305F280208265F2D02656E5F5600DFEE2300FFEE01009F2608FAF1CDB26BD2D6489F2701809F100706020103A0A004DFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}   |
# | Master                      | 541333XXXXXX0483 | 5413330089010483 | 25/12/31       | null      | ;2223000010232012=19122010000000007400? | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994900810008C002A14F00500A4D617374657243617264DFEF5D115413CCCCCCCC0483D2512201CCCCCCCCCC5718EE1B08AEEC88E00BE939E346020282AF70D01485E8DF3DC9DFEF5B085413CCCCCCCC04835A1060B6DFD713F6227D6C94CA852EDE8686820258008407A00000000410108701018A025A33950542200080009A030001019B02E8009C01005F24032512315F2A0208405F300202015F3401499F02060000000001009F03060000000000009F0607A00000000410109F0702FF009F080200029F090200029F1101009F0D05FC50A000009F0E0500000000009F0F05F870A498009F12009F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34035E03009F3501219F360200029F3704A468BBCD9F38009F3901059F3C009F4005F000F0A0019F4104000004479F5301529F6E009F7C005F2011496E7465726F7065722E203034203133415F280200565F2D005F5600DFEE2300FFEE01009F26084FAF1886DA16FE489F2701809F10120212A0000F240000000000000000000000FFDFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}   |
# | Discover                    | 651000XXXXXX0133 | 6510000000000133 | 17/12/31       | null      | ;2223000010232012=19122010000000007400? | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994999190003C002624F005008444953434F564552DFEF5D136510CCCCCCCC0133D1712201CCCCCCCCCCCCCC57183F58AF726A0E9D9358BC105241680D53EE68F74B19A1014BDFEF5B086510CCCCCCCC01335A10A14FE3BFB4FC4337741F4C5160378342820239008407A00000015230108701018A025A33950500200080009A030001019B02E8009C01005F24031712315F2A0208405F300202015F3401019F02060000000001009F03060000000000009F0607A00000015230109F0702FF009F080200019F090200019F1101009F0D05F040C428009F0E0500100000009F0F05F068DCF8009F12009F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34035E03009F3501219F360200039F370418B3F1479F38009F3901059F3C009F4005F000F0A0019F4104000005269F5301529F6E009F7C005F200D434152442F494D4147452031335F280208405F2D005F5600DFEE2300FFEE01009F26085DEE62FF3816897D9F2701809F10080106A04003000000DFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}  |

  @post-CDAPI_IDTechAugusta_FallBackSwipe_EMVSwipedData_NegativeTest_byInvalidData
  Scenario Outline: Verify post CDAPI by for IDTechAugusta FallBackSwipe for final attempt NegativeTest by Invalid Data
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to <deviceData>
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400
  # And response body path should contain no content
    And response body path should not contain "DeviceType":"IDTech"
    And response body path should not contain "EmvData":null
    And response body path should not contain "EmvTags":null
    And response body path should not contain "SwipeData": {
    And response body path should not contain "EmvError": {
    And response body path should not contain "DeviceData": {
  Examples:
  | cardName                    | deviceData |
  | Visa                        | {"deviceData": "TestDFEE250230029F390180DFEE238201CA02BE01801F402800A39B%*4176********0019^PVTDEMOCARD^*******************************?*;4176********0019=********************?*6DB89E7051B95364138906964D61F5AECAEF81534EAD0511B953E0B3F6D3F538BAD5DD1C6DB19383820D528605423E39E60D316CE9D74F9B8E39391E5D411D690427379DA50F1B18F5B8C0F45D98047260E937399E891B92C873D4BF1ED5F759955062484305B53C000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C002A26D6B03"}   |
# | Master Debit                | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994900810008C002A64F07A000000004101050104465626974204D617374657243617264DFEF5D105128CCCCCCCC0002D1908622CCCCCCCC5718FBF7F24F06155FE8EB64EFD1463D085888E00BC844B327A8DFEF5B085128CCCCCCCC00025A10F8BDF7FF2DC5DF112D26943933D1C1FD820239008407A00000000410108701018A025A33950504200080009A030001019B02E8009C01005F24031908315F2A0208405F30005F3401009F02060000000001009F03060000000000009F0607A00000000410109F070229009F080200029F090200029F1101019F0D05BC50BC08009F0E0500000000009F0F05BC70BC98009F12104465626974204D6173746572436172649F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34031E03009F3501219F3602013F9F37042EF30A8C9F38009F3901059F3C009F4005F000F0A0019F4104000004509F5301529F6E009F7C005F201A2F434849502054455354204341524420202020202020202020205F280208405F2D02656E5F5600DFEE2300FFEE01009F260885D23B09817E55149F2701809F10120110A00005220000000000000000000000FFDFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}   |
  | MASTER CARD BIN-2 SERIES    | {"deviceData": "TestDFEE250230029F390180DFEE238201C702BB01801F3D2800A39B%*2223********2012^TEST CARD/EMV BIN-2^********************?*;2223********2012=********************?*4EC25EDBE95D82724C88412C6A62FFE8438942DD4D1766F2465A4C464D371EAE060595C1C0FC0073424A8FC9566314CEBC26654FA189C3B6BF1A2269E8E9683A83657A4E692C73A44A76439E3285999E8BF0FE7C94EFDB036C7ECB2485CAD46AECBBA0E23862A505000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C002A35BEF03"}   |
# | American Express            | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994900810008C002394F08A0000000250108015010414D45524943414E2045585052455353DFEF5D133742CCCCCCC1007D2001201CCCCCCCCCCCCCCC5718F251E8F17B3AA91E1E1AA588042D2DAF1068580FDC1EDFC9DFEF5B083742CCCCCCC1007F5A106E5DE8F2C5E59CEA0AECB74D6032B5AA82023C008408A0000000250108018701018A025A33950508200080009A030001019B02E8009C01005F24032001315F2A0208405F300202015F3401009F02060000000001009F03060000000000009F0606A000000025019F0702FF009F080200019F090200019F1101009F0D05BC50ECA8009F0E0500000000009F0F05BC78FCF8009F12009F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34035E03009F3501219F360200049F3704724905B29F38009F3901059F3C009F4005F000F0A0019F4104000003559F5301529F6E009F7C005F201041454950532032302F56455220322E305F280208265F2D02656E5F5600DFEE2300FFEE01009F2608FAF1CDB26BD2D6489F2701809F100706020103A0A004DFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}   |
# | Master                      | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994900810008C002A14F00500A4D617374657243617264DFEF5D115413CCCCCCCC0483D2512201CCCCCCCCCC5718EE1B08AEEC88E00BE939E346020282AF70D01485E8DF3DC9DFEF5B085413CCCCCCCC04835A1060B6DFD713F6227D6C94CA852EDE8686820258008407A00000000410108701018A025A33950542200080009A030001019B02E8009C01005F24032512315F2A0208405F300202015F3401499F02060000000001009F03060000000000009F0607A00000000410109F0702FF009F080200029F090200029F1101009F0D05FC50A000009F0E0500000000009F0F05F870A498009F12009F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34035E03009F3501219F360200029F3704A468BBCD9F38009F3901059F3C009F4005F000F0A0019F4104000004479F5301529F6E009F7C005F2011496E7465726F7065722E203034203133415F280200565F2D005F5600DFEE2300FFEE01009F26084FAF1886DA16FE489F2701809F10120212A0000F240000000000000000000000FFDFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}   |
# | Discover                    | {"deviceData": "DFEE25020203DFEE26022000DFEE120A62994999190003C002624F005008444953434F564552DFEF5D136510CCCCCCCC0133D1712201CCCCCCCCCCCCCC57183F58AF726A0E9D9358BC105241680D53EE68F74B19A1014BDFEF5B086510CCCCCCCC01335A10A14FE3BFB4FC4337741F4C5160378342820239008407A00000015230108701018A025A33950500200080009A030001019B02E8009C01005F24031712315F2A0208405F300202015F3401019F02060000000001009F03060000000000009F0607A00000015230109F0702FF009F080200019F090200019F1101009F0D05F040C428009F0E0500100000009F0F05F068DCF8009F12009F1A0208409F1E085465726D696E616C9F21030000009F33036028C89F34035E03009F3501219F360200039F370418B3F1479F38009F3901059F3C009F4005F000F0A0019F4104000005269F5301529F6E009F7C005F200D434152442F494D4147452031335F280208405F2D005F5600DFEE2300FFEE01009F26085DEE62FF3816897D9F2701809F10080106A04003000000DFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036"}  |


    # ***********************************************

  @post-CDAPI_IDTechAugusta_FallBackSwipe_EmptyCandidateList_FirstAttempt
  Scenario: Verify post CDAPI for IDTechAugusta FallBackSwipe due to empty candidate list for First Attempt
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "DFEF6102F221" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path SwipeData should be null
    And response body path EmvData.Brand should be null
    And response body path EmvData.MaskedCardnumber should be null
    And response body path EmvData.Cardnumber should be null
    And response body path EmvData.ExpirationDate should be null
    And response body path EmvError.EmvErrorMessage should be EmptyCandidateList
    And response body path EmvError.FrontEndPrompt should be PromptforSwipe
    And response body path DeviceData.KernelVersion should be 312E31302E303337
    And response body path DeviceData.DeviceNumber should be 43234646
    And response body path OfflineDecline should be null
    And response body path should contain "EmvTags":[{"Name":"DFEF61","Length":2,"Value":"F221"}]

  @post-CDAPI_Augusta_FallBackSwipe_EmptyCandidateList_FirstAttempt_NegativeTest_bySmallLetters
  Scenario: Verify post CDAPI for Augusta FallBackSwipe due to empty candidate list for First Attempt NegativeTest_bySmallLetters
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "DFEF6102F221" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path SwipeData should be null
    And response body path EmvData.Brand should be null
    And response body path EmvData.MaskedCardnumber should be null
    And response body path EmvData.Cardnumber should be null
    And response body path EmvData.ExpirationDate should be null
    And response body path EmvError should be null
    And response body path EmvError.FrontEndPrompt should be PromptforSwipe
    And response body path DeviceData.KernelVersion should be 312E31302E303337
    And response body path DeviceData.DeviceNumber should be 43234646
    And response body path OfflineDecline should be null
    And response body path should not contain "EmvTags":[{"Name":"dfef61","Length":2,"Value":"f221"}]

  @post-CDAPI_Augusta_FallBackSwipe_EmptyCandidateList_FirstAttempt_NegativeTest_byInvalidData
  Scenario: Verify post CDAPI for Augusta FallBackSwipe due to empty candidate list for First Attempt NegativeTest byInvalidData
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "TestDFEF6102F221" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400
    # And response body path should contain no content
    And response body path should not contain "DeviceType":"IDTech"
    And response body path should not contain "EmvData":null
    And response body path should not contain "EmvTags":null
    And response body path should not contain "SwipeData": {
    And response body path should not contain "EmvError": {
    And response body path should not contain "DeviceData": {
    And response body path should not contain "EmvTags":[{"Name":"dfef61","Length":2,"Value":"f221"}]

    # ****************** IDTechAugusta_FallBackSwipe_EmptyCandidatelist **************************

  @post-CDAPI_IDTechAugusta_FallBackSwipe_EmptyCandidatelist_EMVSwipedData
  Scenario: Verify post CDAPI by for IDTechAugusta FallBackSwipe for Empty Candidate list EMVSwipedData
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "DFEE250250059F390180DFEE238201E102D501801F472800A39B%*4506********1933^CHASE PAYMENTECH ^********************************?*;4506********1933=********************?*1B8F14907451665D94B5654B684071737F385F8687D408FC223E8B74F18B5F5A46BB86CBB77A51F273425221E0C6EE6908B8F641672737E904A39623ECD91E4DA0C5FC5E0D7F9082377A1E8A4913657F6DEB584C87ADA42DAF429BD9AB9389C0D8DB766244E179055E47D58AAE024E740000000000000000000000000000000000000000000000000000000000000000000000000000000037303754333136343238629949968600000000BF32E403" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path EmvData should be null
    And response body path EmvTags should be null
    And response body path SwipeData.HasChip should be true
    And response body path SwipeData.Track1 should be null
    And response body path SwipeData.Track2 should be ;4506445006931933=15122201583010000001?
    And response body path SwipeData.MaskedCardnumber should be 450644XXXXXX1933
    And response body path SwipeData.Cardnumber should be 4506445006931933
    And response body path SwipeData.ExpirationDate should be 15/12
    And response body path SwipeData.FallbackReason should be EmptyCandidateList
    And response body path SwipeData.FrontEndPrompt should be null
    And response body path EmvError should be null
    And response body path DeviceData.KernelVersion should be 312E31302E303337
  #  And response body path DeviceData.DeviceNumber should be ""
    And response body path OfflineDecline should be null

  @post-CDAPI_IDTechAugusta_FallBackSwipe_EmptyCandidatelist_EMVSwipedData
  Scenario: Verify post CDAPI by for IDTechAugusta FallBackSwipe for Empty Candidate list EMVSwipedData
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to { "deviceData": "TestDFEE250250059F390180DFEE238201E102D501801F472800A39B%*4506********1933^CHASE PAYMENTECH ^********************************?*;4506********1933=********************?*1B8F14907451665D94B5654B684071737F385F8687D408FC223E8B74F18B5F5A46BB86CBB77A51F273425221E0C6EE6908B8F641672737E904A39623ECD91E4DA0C5FC5E0D7F9082377A1E8A4913657F6DEB584C87ADA42DAF429BD9AB9389C0D8DB766244E179055E47D58AAE024E740000000000000000000000000000000000000000000000000000000000000000000000000000000037303754333136343238629949968600000000BF32E403" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400
    # And response body path should contain no content
    And response body path should not contain "DeviceType":"IDTech"
    And response body path should not contain "SwipeData": {
    And response body path should not contain "EmvError":null
    And response body path should not contain "DeviceData": {


