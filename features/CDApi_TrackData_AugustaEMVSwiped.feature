Feature: Card Decrypt API - Verify CD API functionality with tracking data for IDTech Augusta EMV Swiped
         for Visa, Amex, Master, Discover cards

  @post-CDAPI_IDTechAugusta_SwipedData_For_AllCards
  Scenario Outline: Verify post CDAPI by Valid IDTech Augusta Swiped Data for all Cards
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to <deviceData>
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path EmvData should be null
    And response body path EmvTags should be null
    And response body path SwipeData.HasChip should be true
  # And response body path SwipeData.IsFallback should be false
    And response body path SwipeData.MaskedCardnumber should be <maskedCardnumber>
    And response body path SwipeData.Cardnumber should be <cardNumber>
    And response body path SwipeData.ExpirationDate should be <expirationDate>
    And response body path EmvError should be null
    Examples:
    | cardName          | maskedCardnumber | cardNumber       | expirationDate | deviceData |
    | Visa              | 417666XXXXXX0019 | 4176662220000019 | 14/12          | { "deviceData": "02BE01801F402800A39B%*4176********0019^PVTDEMOCARD^*******************************?*;4176********0019=********************?*947D512412D8B4C38D2F5F0E2C3576F4167D4854240AE3FF8D9DAC7CB0828E1BC48F9C9E571B400DC165ABBACB713C8755C5FC1265EEECCC1F63445C7371E8425F1893A4DD03BA64F8A90DF4825BF7800423B5808C7A6667EA70B9F6EBD646A928CAB07780965184000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C0028114FC03" }  |
    | Master            | 222300XXXXXX2012 | 2223000010232012 | 19/12          | {"deviceData": "02BB01801F3D2800A39B%*2223********2012^TEST CARD/EMV BIN-2^********************?*;2223********2012=********************?*B91D1007D04A70A97078640C82C0FFA058C3B657DBBB1BEDA961707DE15A5E517D562ACA3E392C944ABFF4EA08D12BC1109B6B80B7AD0D3EA0C16282D388E9B47DA4A95BA518B5F59641356BD9D0DCF97A4A63FD1E0AF7D9C9927E82532D373B3257D99DFE4893FF000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C00282582403"}   |
   # | American Express  | 371449XXXXX8431  | 371449635398431  | 18/12          | {"deviceData": "02A301801F3A2700039B%*3714*******8431^CHASE PAYMENTECH ^********************?*;3714*******8431=********************?*A2B60C4826F2DCC23154D5DCA35CE41254CCF70065376B47D69656553928EF07F0AECBDBF6579AD7FE92E5C5621CFF9EA66D0D6C321995B321BD6598CA2EDE6996C0FCC5CC474A92F6468696B129E28C847F563E6112115369FD902927073B7EF681513DEB8049704901C4B6037A6E21F5BBDA59D1928D60F1ACCA05D3CEE053B2549390A352BB6BEF9C8C09A421AF7C62994900810008A00018453D03"}   |
   # | Discover          | 601100XXXXXX0000 | 6011000995500000 | 18/12          | {"deviceData": "02A601801F3C2800039B%*6011********0000^ CHASE PAYMENTECH ^********************?*;6011********0000=********************?*C6BFCE76A9BD6C168C0B44185650F9BC6FBBA8CCAA204A05FDC8B30F380D5488DE67B85877126DB52551CBF3B12D62C4AC291941254299A442419022178F660C1D3571DC2D4C1793F46714D12065EE7828A786B05E71B89DAAF2D348E8B0030F055B35ACC592A91D669F3779A45C82C0D39CB4729EBF6E132E79492DB6AEC36475D6DA00A1EE9FA45B0E094F98D9F8DC62994900810008A000196E7E03"}  |

  @post-CDAPI_ValidateTracl1Track2Data_For_AllCards_ByIDTechAugustaData
  Scenario Outline: Verify Tracl1 Track2 Data for all Cards using IDTechAugusta Swiped Data
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to <deviceData>
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 200
    And response body path DeviceType should be IDTech
    And response body path EmvData should be null
    And response body path EmvTags should be null
    And response body path SwipeData.Track1 should be <Track1>
    And response body path SwipeData.Track2 should be <Track2>
    Examples:
    | cardName          |  Track1 | Track2 | deviceData |
    | Visa              | null  | ;4176662220000019=14122011000099999991?  | {"deviceData": "02BE01801F402800A39B%*4176********0019^PVTDEMOCARD^*******************************?*;4176********0019=********************?*947D512412D8B4C38D2F5F0E2C3576F4167D4854240AE3FF8D9DAC7CB0828E1BC48F9C9E571B400DC165ABBACB713C8755C5FC1265EEECCC1F63445C7371E8425F1893A4DD03BA64F8A90DF4825BF7800423B5808C7A6667EA70B9F6EBD646A928CAB07780965184000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C0028114FC03" }  |
    | Master            | null  | ;2223000010232012=19122010000000007400?  | {"deviceData": "02BB01801F3D2800A39B%*2223********2012^TEST CARD/EMV BIN-2^********************?*;2223********2012=********************?*B91D1007D04A70A97078640C82C0FFA058C3B657DBBB1BEDA961707DE15A5E517D562ACA3E392C944ABFF4EA08D12BC1109B6B80B7AD0D3EA0C16282D388E9B47DA4A95BA518B5F59641356BD9D0DCF97A4A63FD1E0AF7D9C9927E82532D373B3257D99DFE4893FF000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C00282582403"}   |
   # | American Express  | %B371449635398431^CHASE PAYMENTECH ^18121015432112345678?            | null                                     | {"deviceData": "020401801D3A00000189%*3714*******8431^CHASE PAYMENTECH ^********************?*3749471B28D315219F3E71112099B0108BB25F13FC49EEBE6E6C8140FC85A4140CA5E93F14F5F7A054C2716F3E678D3876B2395D7D0400536F70037BC881ED434901C4B6037A6E21F5BBDA59D1928D60F1ACCA0562994900810008A0022E788E03"}   |
   # | Discover          | %B6011000995500000^ CHASE PAYMENTECH ^18121015432112345678?          | ;6011000995500000=18121015432112345678?  | {"deviceData": "02A601801F3C2800039B%*6011********0000^ CHASE PAYMENTECH ^********************?*;6011********0000=********************?*3D2C5D8D6BD3D3F2887F92001D87C55311A1312FD05C4A498C527DACE8190C784E204AD2FE49AEB99A494E968097E1330BF93643A5ACC422788E011EC8C407980F52E81E1F93D4CC280B4A6FC503E17D972511EE7D387AC213F5DD903F392F7D7D598D7B2D2DA226669F3779A45C82C0D39CB4729EBF6E132E79492DB6AEC36475D6DA00A1EE9FA45B0E094F98D9F8DC62994900810008A0022F6DC703"}  |


  @post-CDAPI_ByInvalid_IDTechAugustaData_For_AllCards
  Scenario Outline: Verify post CDAPI by Invalid IDTechAugusta Swiped Data for all Cards
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to <deviceData>
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400
#   And response header content-Type should be null
    And response header content-length should be 0
    Examples:
    | cardName          |  deviceData |
    | Visa              |  { "deviceData": "**Test**02BE01801F402800A39B%*4176********0019^PVTDEMOCARD^*******************************?*;4176********0019=********************?*947D512412D8B4C38D2F5F0E2C3576F4167D4854240AE3FF8D9DAC7CB0828E1BC48F9C9E571B400DC165ABBACB713C8755C5FC1265EEECCC1F63445C7371E8425F1893A4DD03BA64F8A90DF4825BF7800423B5808C7A6667EA70B9F6EBD646A928CAB07780965184000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C0028114FC03" }  |
    | Master            |  {"deviceData": "02BB01801F3D28%%Test%%%00A39B%*2223********2012^TEST CARD/EMV BIN-2^********************?*;2223********2012=********************?*B91D1007D04A70A97078640C82C0FFA058C3B657DBBB1BEDA961707DE15A5E517D562ACA3E392C944ABFF4EA08D12BC1109B6B80B7AD0D3EA0C16282D388E9B47DA4A95BA518B5F59641356BD9D0DCF97A4A63FD1E0AF7D9C9927E82532D373B3257D99DFE4893FF000000000000000000000000000000000000000000000000000000000000000000000000000000003631335435333535333762994900810008C00282582403"}   |
  #  | American Express  |  {"deviceData": "02A3018###Test###01F3C2800039B%*6011********0000^ CHASE PAYMENTECH ^********************?*;6011********0000=********************?*C6BFCE76A9BD6C168C0B44185650F9BC6FBBA8CCAA204A05FDC8B30F380D5488DE67B85877126DB52551CBF3B12D62C4AC291941254299A442419022178F660C1D3571DC2D4C1793F46714D12065EE7828A786B05E71B89DAAF2D348E8B0030F055B35ACC592A91D669F3779A45C82C0D39CB4729EBF6E132E79492DB6AEC36475D6DA00A1EE9FA45B0E094F98D9F8DC62994900810008A000196E7E03"}  |
  #  | Discover          |  {"deviceData": "02A601801F3C2800039B%*6011********0000^ CHASE  Test PAYMENTECH ^********************?*;6011********0000=********************?*C6BFCE76A9BD6C168C0B44185650F9BC6FBBA8CCAA204A05FDC8B30F380D5488DE67B85877126DB52551CBF3B12D62C4AC291941254299A442419022178F660C1D3571DC2D4C1793F46714D12065EE7828A786B05E71B89DAAF2D348E8B0030F055B35ACC592A91D669F3779A45C82C0D39CB4729EBF6E132E79492DB6AEC36475D6DA00A1EE9FA45B0E094F98D9F8DC62994900810008A000196E7E03"}  |