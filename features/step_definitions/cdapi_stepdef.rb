

Given(/^I set Authorization header with MID as (.*) and MKEY as (.*)$/) do |mid,mkey|
@cdapi_test.set_authorization mid , mkey
end

And(/^I set url as (.*)$/) do |url|
  @cdapi_test.set_url url
end

And(/^I set (.*) header to (.*)$/) do |header,value|
  @cdapi_test = CDApiTest.new
  @cdapi_test.set_header header , value
end

And(/^I set body to (.*)$/) do |json|
  @cdapi_test.set_json_body json
end

And(/^I post data for transaction (.*)$/) do |path|
  @cdapi_test.post path
end
And(/^I patch data for transaction (.*)$/) do |path|
  @cdapi_test.patch path
end

And(/^response header (.*) should be (.*)$/) do |header_param,val|
  @cdapi_test.verify_response_headers header_param , val
end

And(/^response body path (.*) should be (.*)$/) do |param,val|
  @cdapi_test.verify_response_params param , val
end

And(/^verify response body path for (.*) with name "(.*)" and value "(.*)" and length (.*)$/) do |emvtags, name, value, length|
  @cdapi_test.verify_emvtags name, value, length
end

And(/^response code should be (.*)$/) do |responsecode|
  @cdapi_test.verify_response_params "code" , responsecode
end

And(/^verify all emv tags from cd api response body with (.*)$/) do |tags|
  @cdapi_test.verify_all_emvtags tags
end

#And(/^response body path should not contain emvTag (.*)$/) do |tagName|
 # @cdapi_test.verify_emvtag_notexist tagName
#end

#And(/^response body path should contain emvTag (.*)$/) do |tagName|
 # @cdapi_test.verify_emvtag_exist tagName
#end

And(/^response body path should contain (.*)$/) do |value|
  @cdapi_test.verify_emvtag_exist value
end

And(/^response body path should not contain (.*)$/) do |value|
  @cdapi_test. verify_emvtag_notexist value
end

And(/^verify all the list of emv tags for Augusta OfflineDecline track data$/) do |table|
 # @cdapi_test.verify_emvtag_exist tagName
    table.hashes.each do |row|
    #  puts row['TagName']
      @cdapi_test.verify_emvtag_exist row['TagName']
    end
  end
