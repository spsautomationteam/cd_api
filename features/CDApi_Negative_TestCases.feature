Feature: Card Decrypt API - Verify CD API functionality with Negative Test Cases

  @Post_CDAPI_With_NotEnclosing_CardTrackData_AtJsonBody
  Scenario: Verify post CDAPI by Not enclosing Card data
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to {"deviceData": 029001801F362800039B%*4788********8291^ENGLISH/VISA^********************?*;4788********8291=********************?*6E6357B8145C78C75F4761EB0D41815936418EB6550247B45E14BC96CE3E67DB616837A9A6C2CC22CA59669E16027ADC8D8EE3588B76743BE0918794D5093371BCE35DAFA52E1D5B3CD46D3881D82E06BFF197058B3D5013497B221A092D6A46F116C5DED8D608CB0C09BF32F5D075C0CC3A6910BC3CC69967E81B5F4747AB488CF62C4D80BE091A62994900810008A000160BA503 }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500
    And response body path Message should be An error has occurred.

  @Post_CDAPI_WithInvalid_LabelName_AtJsonBody
  Scenario: Verify post CDAPI by Not enclosing Card data
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to {"Test": "DFEE25020001DFEE26022000DFEE120A62994999190003C0023B4F00500B5649534120435245444954DFEF5B084761CCCCCCCC00105A108317DF04C1F806142C7F5D3394A1D9C55F2A0208405F3401015F57010082025C008A00950502B00080009A030001019B02C8009C01009F02060000000001009F03060000000000009F0607A00000000310109F0702AB809F0D0500000000009F0E0500000000009F0F0500000000009F100706010A038000009F1101019F120F4352454449544F20444520564953419F1A0208409F21030000009F2608972D12ADD1E76B439F2701009F34031E03009F360200039F3704CFEAF31F9F390105DF1305DC4000A800DF14050010000000DF1505DC4004F800DFEF5720494420544543482041756775737461205553422D4B422056312E30322E303036" }
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500
 #  And response body path should contain no content

  @Post_CDAPI_ByWithout_Enclosing_CardTrackData_WithBraces
  Scenario: Verify post CDAPI By Without enclosing CardTrackData With Braces
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to "deviceData": 029001801F362800039B%*4788********8291^ENGLISH/VISA^********************?*;4788********8291=********************?*6E6357B8145C78C75F4761EB0D41815936418EB6550247B45E14BC96CE3E67DB616837A9A6C2CC22CA59669E16027ADC8D8EE3588B76743BE0918794D5093371BCE35DAFA52E1D5B3CD46D3881D82E06BFF197058B3D5013497B221A092D6A46F116C5DED8D608CB0C09BF32F5D075C0CC3A6910BC3CC69967E81B5F4747AB488CF62C4D80BE091A62994900810008A000160BA503
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500
    And response body path Message should be An error has occurred.

  @Post_CDAPI_BywithOut_JsonbodySyntax
  Scenario: Verify post CDAPI with Card data and without Json body syntax
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to 029001801F362800039B%*4788********8291^ENGLISH/VISA^********************?*;4788********8291=********************?*6E6357B8145C78C75F4761EB0D41815936418EB6550247B45E14BC96CE3E67DB616837A9A6C2CC22CA59669E16027ADC8D8EE3588B76743BE0918794D5093371BCE35DAFA52E1D5B3CD46D3881D82E06BFF197058B3D5013497B221A092D6A46F116C5DED8D608CB0C09BF32F5D075C0CC3A6910BC3CC69967E81B5F4747AB488CF62C4D80BE091A62994900810008A000160BA503
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500
    And response body path Message should be An error has occurred.

  @Post_CDAPI_With-Blank-DeviceData
  Scenario: Verify post CDAPI by Blank DeviceData
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to {"deviceData": ""}
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500

  @Post_CDAPI_WithOut-DeviceData
  Scenario: Verify post CDAPI by WithOut Device Data
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to {}
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500

  @Post_CDAPI_With_Invalid_DeviceData
  Scenario: Verify post CDAPI with Invalid DeviceData
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to {"deviceData": "abcdTest"}
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 400

  @Post_CDAPI_With_Invalid_JsonBody
  Scenario: Verify post CDAPI with Invalid JsonBody
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to "Test"
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500
    And response body path Message should be An error has occurred.

  @Post_CDAPI_With_Wrong_JsonBody_Test
  Scenario: Verify post CDAPI with Wrong JsonBody
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to Test
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500
    And response body path Message should be An error has occurred.

  @Post_CDAPI_ByPutting_DoubleQuotes_IntoJsonBody
  Scenario: Verify post CDAPI with Invalid JsonBody
    Given I set content-type header to application/json
    And I set deviceType header to IDTech
    And I set body to ""
    When I post data for transaction /Decrypt?deviceType=IDTech
    Then response code should be 500
    And response body path Message should be An error has occurred.